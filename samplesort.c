#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <inttypes.h>
#include "cse6230rand.h"
#include "proj2sorter.h"
#include "proj2sorter_impl.h"

/* A basic strategy to choose a pivot is to have the root broadcast its
 * median entry, and hope it will be close to the median for all processes */
static int ChoosePivots(Proj2Sorter sorter, MPI_Comm comm, int size, int rank, size_t numKeysLocal, uint64_t *keys, uint64_t *pivot_p, uint64_t *sample, cse6230nrand_t nrand)
{
  int      err;
  size_t i = 0, in_flag = 1, r_arr_pos = 0, j;
  double nrval;
  uint64_t rval = -1;
  uint64_t *rval_arr=NULL; 

  //err = PROJ2MALLOC(size-1, &rval_arr); PROJ2CHK(err);

  if (numKeysLocal) 
    for(i=1; i<size; i++){
      // printf("Choosing pivot %d\n", (i*(((numKeysLocal - size + 1)/size) + 1)) - 1);
      pivot_p[i-1] = keys[ (i*(((numKeysLocal - size + 1)/size) + 1)) - 1];
      /*
      in_flag = 1;
      while(in_flag){
	while(rval<0 || rval>(numKeysLocal - 1)){
          nrval = cse6230nrand(&nrand);
	  rval = llrint((nrval * ((numKeysLocal/2)/3)) + (numKeysLocal/2));
	}
	in_flag=0;
	for(j=0;j<r_arr_pos;j++){
	  if(rval_arr[j]==rval){
	    in_flag = 1;
	    break;
	  }
	}
	if(in_flag==0){
	  rval_arr[r_arr_pos] = rval;
	  r_arr_pos++;
	  if(r_arr_pos==size) printf("last!\n");
	}
      }
      //printf("rank: %d, i: %lu, rval: %lu\n", rank, i, rval);
      pivot_p[i-1] = keys[(size_t)rval];
      rval = -1;
      */
    }
  err = MPI_Gather(pivot_p, size-1, MPI_UINT64_T, sample, size-1, MPI_UINT64_T, 0, comm); MPI_CHK(err);
  //err = PROJ2FREE(&rval_arr); PROJ2CHK(err);
  return 0;
}

/* instead of finding a perfect match, use this comparison operation to find
 * when a key fits between two entries in an array */
static int uint64_compare_pair(const void *key, const void *array)
{
  uint64_t Key = *((const uint64_t *) key);
  const uint64_t *pair = (const uint64_t *) array;
  if (Key < pair[0]) return -1;
  if (Key < pair[1]) return 0;
  return 1;
}

static int Proj2SorterSort_samplesort_recursive(Proj2Sorter sorter, MPI_Comm comm, size_t numKeysLocal, uint64_t *keys, cse6230nrand_t nrand)
{
  uint64_t *pivot = NULL;
  uint64_t *sample=NULL;
  uint64_t *splitters=NULL;
  uint64_t **buckets = NULL;
  uint64_t *bucket_size = NULL, *cum_bucket_size=NULL;
  uint64_t *bucket_size_new = NULL, *cum_bucket_size_new=NULL;
  uint64_t *keysNew;
  size_t    numKeysLocalNew=0;
  int      size, rank;
  int      err;
  size_t i;

  err = MPI_Comm_size(comm, &size); PROJ2CHK(err);
  err = MPI_Comm_rank(comm, &rank); PROJ2CHK(err);
  /* sort locally up front */
  err = Proj2SorterSortLocal(sorter, numKeysLocal, keys, PROJ2SORT_FORWARD); PROJ2CHK(err);
  if (size == 1) {
    /* base case: nothing to do */
    return 0;
  }
  err = PROJ2MALLOC(size-1, &pivot); PROJ2CHK(err);
  err = PROJ2MALLOC(size*(size-1), &sample); PROJ2CHK(err);
  err = PROJ2MALLOC(size-1, &splitters); PROJ2CHK(err);
  err = PROJ2MALLOC(size, &buckets); PROJ2CHK(err);
  err = PROJ2MALLOC(size, &bucket_size); PROJ2CHK(err);
  err = PROJ2MALLOC(size, &cum_bucket_size); PROJ2CHK(err);
  err = PROJ2MALLOC(size, &bucket_size_new); PROJ2CHK(err);
  err = PROJ2MALLOC(size, &cum_bucket_size_new); PROJ2CHK(err);

  err = ChoosePivots(sorter, comm, size, rank, numKeysLocal, keys, pivot, sample, nrand); PROJ2CHK(err);
  if(rank==0) {
    err = Proj2SorterSortLocal(sorter, size*(size-1), sample, PROJ2SORT_FORWARD); PROJ2CHK(err);
    for(i=1; i<size; i++){
      splitters[i-1] = sample[ (i*(((size*(size-1) - size + 1)/size) + 1)) - 1];
    }
    // for(i=0; i<(size-1); i++)
    //   printf("rank: %d, splitters[%d]: %" PRIu64 " ", rank, i, splitters[i]);
  }
  // printf("\n");
  err = MPI_Bcast(splitters, size-1, MPI_UINT64_T, 0, comm); MPI_CHK(err);
  for(i=0; i<size; i++){
    bucket_size[i] = 0;
    cum_bucket_size[i] = 0;
    buckets[i] = NULL;
  }


  /* split the keys into those less than and those greater than the pivot */
  if (numKeysLocal) {
    if(splitters[0] >= keys[numKeysLocal - 1]){
      buckets[0] = keys;
      bucket_size[0] = numKeysLocal;
    }
    else if(splitters[size-2] < keys[0]){
      buckets[size-1] = keys;
      bucket_size[size-1] = numKeysLocal;
    }
    else{
      buckets[0] = keys;
      int flg=0;
      uint64_t prev_cum;
      for(i=0;i<size-1;i++){
        prev_cum = (i==0)?0:cum_bucket_size[i-1];
        buckets[i+1] = (uint64_t *) bsearch(&splitters[i], keys + prev_cum, numKeysLocal - prev_cum, sizeof(uint64_t), uint64_compare_pair);
        // printf("rank: %d, buckets[%d]: %" PRIu64 " at index: %d\n", rank, i+1, *buckets[i+1], buckets[i+1] - keys);
        if (!buckets[i+1]) {
          bucket_size[i] = 0;
          cum_bucket_size[i] = prev_cum + bucket_size[i]; 
          buckets[i+1] = buckets[i];
          buckets[i] = NULL;
        }
        if(buckets[i+1] == keys + numKeysLocal - 1){
          bucket_size[i] = buckets[i+1] - buckets[i] + 1;
          for(int j=i; j<size; j++) cum_bucket_size[j] = prev_cum + bucket_size[i]; 
          flg=1;
          buckets[i+1] = NULL;
          break;
        }
        else{
          buckets[i+1]++;
          bucket_size[i] = buckets[i+1] - buckets[i];
          cum_bucket_size[i] = prev_cum + bucket_size[i]; 
        }
        // printf("rank: %d, bucket_size[%d]: %" PRIu64 ", cum_bucket_size[%d]: %" PRIu64 "\n", rank, i, bucket_size[i], i, cum_bucket_size[i]);
      }
      if(flg==0){
  	bucket_size[size-1] = numKeysLocal - cum_bucket_size[size-2];
	cum_bucket_size[size-1] = numKeysLocal;
      }
      // printf("rank: %d, bucket_size[%d]: %" PRIu64 ", cum_bucket_size[%d]: %" PRIu64 "\n", rank, i, bucket_size[i], i, cum_bucket_size[i]);
    }
  }
  
  err = MPI_Alltoall(bucket_size, 1, MPI_UINT64_T, bucket_size_new, 1, MPI_UINT64_T, comm); MPI_CHK(err);

  cum_bucket_size_new[0] = 0;
  numKeysLocalNew = bucket_size_new[0];
  for(i=1; i<size; i++) {
    numKeysLocalNew += bucket_size_new[i];
    cum_bucket_size_new[i] = bucket_size_new[i-1] + cum_bucket_size_new[i-1];
    cum_bucket_size[size-i] = cum_bucket_size[size-i-1];
  }
  cum_bucket_size[0] = 0;
  // printf("rank: %d, numkeyslocalnew : %d\n", rank, numKeysLocalNew); 
  // for(i=0;i<size;i++){
  //   printf("rank: %d, bucket_size[%d]: %" PRIu64 ", cum_bucket_size[%d]: %" PRIu64 "\n", rank, i, bucket_size[i], i, cum_bucket_size[i]);
  // }
  // for(i=0;i<size;i++){
  //   printf("rank: %d, bucket_size_new[%d]: %" PRIu64 ", cum_bucket_size_new[%d]: %" PRIu64 "\n", rank, i, bucket_size_new[i], i, cum_bucket_size_new[i]);
  // }

  err = Proj2SorterGetWorkArray(sorter, numKeysLocalNew, sizeof(uint64_t), &keysNew); PROJ2CHK(err);
  int *bucket_size_int = malloc(sizeof(int)*size);
  int *cum_bucket_size_int = malloc(sizeof(int)*size);
  int *bucket_size_new_int = malloc(sizeof(int)*size);
  int *cum_bucket_size_new_int = malloc(sizeof(int)*size);
  for(i=0; i<size; i++){
    bucket_size_int[i] = (int)bucket_size[i];
    cum_bucket_size_int[i] = (int)cum_bucket_size[i];
    bucket_size_new_int[i] = (int)bucket_size_new[i];
    cum_bucket_size_new_int[i] = (int)cum_bucket_size_new[i];
  }
  err = MPI_Alltoallv(keys, bucket_size_int, cum_bucket_size_int, MPI_UINT64_T, keysNew, bucket_size_new_int, cum_bucket_size_new_int, MPI_UINT64_T, comm); MPI_CHK(err);
  err = Proj2SorterSortLocal(sorter, numKeysLocalNew, keysNew, PROJ2SORT_FORWARD); PROJ2CHK(err);
  
  
  /* Now the array is sorted, but we have to move it back to its original
   * distribution */
  {
    uint64_t myOldCount = numKeysLocal;
    uint64_t myNewCount = numKeysLocalNew;
    uint64_t *oldOffsets;
    uint64_t *newOffsets;
    uint64_t oldOffset, newOffset;
    MPI_Request *recv_reqs, *send_reqs;
    int firstRecv = -1;
    int lastRecv = -2;
    int firstSend = -1;
    int lastSend = -2;
    int nRecvs, nSends;
    uint64_t thisOffset;

    err = Proj2SorterGetWorkArray(sorter, size + 1, sizeof(uint64_t), &oldOffsets); PROJ2CHK(err);
    err = Proj2SorterGetWorkArray(sorter, size + 1, sizeof(uint64_t), &newOffsets); PROJ2CHK(err);
    err = MPI_Allgather(&myOldCount,1,MPI_UINT64_T,oldOffsets,1,MPI_UINT64_T,comm); PROJ2CHK(err);
    err = MPI_Allgather(&myNewCount,1,MPI_UINT64_T,newOffsets,1,MPI_UINT64_T,comm); PROJ2CHK(err);

    oldOffset = 0;
    for (int i = 0; i < size; i++) {
      uint64_t count = oldOffsets[i];
      oldOffsets[i] = oldOffset;
      oldOffset += count;
    }
    oldOffsets[size] = oldOffset;

    newOffset = 0;
    for (int i = 0; i < size; i++) {
      uint64_t count = newOffsets[i];
      newOffsets[i] = newOffset;
      newOffset += count;
    }
    newOffsets[size] = newOffset;

    if (myOldCount) {
      for (int i = 0; i < size; i++) {
        if (newOffsets[i] <= oldOffsets[rank] && oldOffsets[rank] < newOffsets[i + 1]) {
          firstRecv = i;
        }
        if (newOffsets[i] <= oldOffsets[rank + 1] - 1 && oldOffsets[rank + 1] - 1 < newOffsets[i + 1]) {
          lastRecv = i + 1;
          break;
        }
      }
    }

    err = Proj2SorterGetWorkArray(sorter, lastRecv - firstRecv, sizeof(*recv_reqs), &recv_reqs); PROJ2CHK(err);

    thisOffset = oldOffsets[rank];
    nRecvs = 0;
    for (int i = firstRecv; i < lastRecv; i++) {
      size_t recvStart = thisOffset;
      size_t recvEnd   = newOffsets[i + 1];

      if (oldOffsets[rank + 1] < recvEnd) {
        recvEnd = oldOffsets[rank + 1];
      }
      if (recvEnd > recvStart) {
        err = MPI_Irecv(&keys[thisOffset - oldOffsets[rank]],(int) (recvEnd - recvStart), MPI_UINT64_T, i, PROJ2TAG_QUICKSORT, comm, &recv_reqs[nRecvs++]); MPI_CHK(err);
      }
      thisOffset = recvEnd;
    }

    if (myNewCount) {
      for (int i = 0; i < size; i++) {
        if (oldOffsets[i] <= newOffsets[rank] && newOffsets[rank] < oldOffsets[i + 1]) {
          firstSend = i;
        }
        if (oldOffsets[i] <= newOffsets[rank + 1] - 1 && newOffsets[rank + 1] - 1 < oldOffsets[i + 1]) {
          lastSend = i + 1;
          break;
        }
      }
    }

    err = Proj2SorterGetWorkArray(sorter, lastSend - firstSend, sizeof(*send_reqs), &send_reqs); PROJ2CHK(err);

    thisOffset = newOffsets[rank];
    nSends = 0;
    for (int i = firstSend; i < lastSend; i++) {
      size_t sendStart = thisOffset;
      size_t sendEnd   = oldOffsets[i + 1];

      if (newOffsets[rank + 1] < sendEnd) {
        sendEnd = newOffsets[rank + 1];
      }
      if (sendEnd > sendStart) {
        err = MPI_Isend(&keysNew[thisOffset - newOffsets[rank]],(int) (sendEnd - sendStart), MPI_UINT64_T, i, PROJ2TAG_QUICKSORT, comm, &send_reqs[nSends++]); MPI_CHK(err);
      }
      thisOffset = sendEnd;
    }

    err = MPI_Waitall(nRecvs, recv_reqs, MPI_STATUSES_IGNORE); PROJ2CHK(err);
    err = MPI_Waitall(nSends, send_reqs, MPI_STATUSES_IGNORE); PROJ2CHK(err);

    err = Proj2SorterRestoreWorkArray(sorter, lastSend - firstSend, sizeof(*send_reqs), &send_reqs); PROJ2CHK(err);
    err = Proj2SorterRestoreWorkArray(sorter, lastRecv - firstRecv, sizeof(*recv_reqs), &recv_reqs); PROJ2CHK(err);
    err = Proj2SorterRestoreWorkArray(sorter, size + 1, sizeof(uint64_t), &newOffsets); PROJ2CHK(err);
    err = Proj2SorterRestoreWorkArray(sorter, size + 1, sizeof(uint64_t), &oldOffsets); PROJ2CHK(err);
  }
  err = Proj2SorterRestoreWorkArray(sorter, numKeysLocalNew, sizeof(uint64_t), &keysNew); PROJ2CHK(err);
  err = PROJ2FREE(&bucket_size); PROJ2CHK(err);
  err = PROJ2FREE(&bucket_size_int); PROJ2CHK(err);
  err = PROJ2FREE(&cum_bucket_size); PROJ2CHK(err);
  err = PROJ2FREE(&cum_bucket_size_int); PROJ2CHK(err);
  err = PROJ2FREE(&bucket_size_new); PROJ2CHK(err);
  err = PROJ2FREE(&bucket_size_new_int); PROJ2CHK(err);
  err = PROJ2FREE(&cum_bucket_size_new); PROJ2CHK(err);
  err = PROJ2FREE(&cum_bucket_size_new_int); PROJ2CHK(err);
  err = PROJ2FREE(&buckets); PROJ2CHK(err);
  err = PROJ2FREE(&splitters); PROJ2CHK(err);
  err = PROJ2FREE(&sample); PROJ2CHK(err);
  err = PROJ2FREE(&pivot); PROJ2CHK(err);
  return 0;
}

int Proj2SorterSort_samplesort(Proj2Sorter sorter, size_t numKeysLocal, uint64_t *keys, cse6230nrand_t nrand)
{
  int      err;

  /* initiate recursive call */
  err = Proj2SorterSort_samplesort_recursive(sorter, sorter->comm, numKeysLocal, keys, nrand); PROJ2CHK(err);
  return 0;
}
