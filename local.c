#include <stdlib.h>
#include "proj2sorter.h"
#include "proj2sorter_impl.h"
#include "proj2.h"
#define SORT_NAME proj2_swenson
#define SORT_TYPE uint64_t
#include "swensonsort/sort.h"


int Proj2SorterSortLocal_swenson_quick_sort(Proj2Sorter sorter, size_t numKeysLocal, uint64_t *keys, int direction)
{
  proj2_swenson_quick_sort(keys, numKeysLocal);
  if (direction == PROJ2SORT_BACKWARD) {
    for (int i = 0; i < numKeysLocal / 2; i++) {
      uint64_t swap = keys[i];
      keys[i] = keys[numKeysLocal - 1 - i];
      keys[numKeysLocal - 1 - i] = swap;
    }
  }
  return 0;
}

int Proj2SorterSortLocal_qsort(Proj2Sorter sorter, size_t numKeysLocal, uint64_t *keys, int direction)
{
  if (direction == PROJ2SORT_FORWARD) {
    qsort(keys, numKeysLocal, sizeof(*keys), uint64_comp_forward);
  } else {
    qsort(keys, numKeysLocal, sizeof(*keys), uint64_comp_backward);
  }
  return 0;
}

void merge (uint64_t *keys_1, size_t numKeysLocal_1, uint64_t *keys_2, size_t numKeysLocal_2, uint64_t *keys) 
{
  size_t i,j,k;
  i = 0; 
  j = 0;
  k = 0;
  while (i < numKeysLocal_1 && j < numKeysLocal_2) {
    if (keys_1[i] <= keys_2[j]) {
    /* copy A[i] to C[k] and move the pointer i and k forward */
    keys[k] = keys_1[i];
    i++;
    k++;
    }
    else {
      /* copy B[j] to C[k] and move the pointer j and k forward */
      keys[k] = keys_2[j];
      j++;
      k++;
    }
  }
  /* move the remaining elements in A into C */
  while (i < numKeysLocal_1) {
    keys[k]= keys_1[i];
    i++;
    k++;
  }
  /* move the remaining elements in B into C */
  while (j < numKeysLocal_2)  {
    keys[k]= keys_2[j];
    j++;
    k++;
  }
}  


int Proj2SorterSortLocal_mergesort(Proj2Sorter sorter, size_t numKeysLocal, uint64_t *keys, int direction)
{
  int err;
  size_t i;
  uint64_t *keys_1, *keys_2;
  size_t numKeysLocal_1, numKeysLocal_2;

  if (numKeysLocal < 2)
    return 0;   

  numKeysLocal_1 = numKeysLocal / 2;   /* the number of elements in A1 */
  numKeysLocal_2 = numKeysLocal - numKeysLocal_1;  /* the number of elements in A2 */
  err = PROJ2MALLOC(numKeysLocal_1, &keys_1); PROJ2CHK(err);
  err = PROJ2MALLOC(numKeysLocal_2, &keys_2); PROJ2CHK(err);
  
  /* move the first n/2 elements to A1 */
  for (i =0; i < numKeysLocal_1; i++) {
    keys_1[i] = keys[i];
  }
  /* move the rest to A2 */
  for (i = 0; i < numKeysLocal_2; i++) {
    keys_2[i] = keys[i + numKeysLocal_1];
  }
  /* recursive call */
  Proj2SorterSortLocal_mergesort(sorter, numKeysLocal_1, keys_1, direction);
  Proj2SorterSortLocal_mergesort(sorter, numKeysLocal_2, keys_2, direction);

  /* conquer */
  merge(keys_1, numKeysLocal_1, keys_2, numKeysLocal_2, keys);
  err = PROJ2FREE(&keys_1); PROJ2CHK(err);
  err = PROJ2FREE(&keys_2); PROJ2CHK(err);
  return 0;
}

int Proj2SorterSortLocal_swenson_merge_sort(Proj2Sorter sorter, size_t numKeysLocal, uint64_t *keys, int direction)
{
  proj2_swenson_merge_sort(keys, numKeysLocal);
  if (direction == PROJ2SORT_BACKWARD) {
    for (int i = 0; i < numKeysLocal / 2; i++) {
      uint64_t swap = keys[i];
      keys[i] = keys[numKeysLocal - 1 - i];
      keys[numKeysLocal - 1 - i] = swap;
    }
  }
  return 0;
}

int Proj2SorterSortLocal(Proj2Sorter sorter, size_t numKeysLocal, uint64_t *keys, int direction)
{
  int err;

#if 0
  err = Proj2SorterSortLocal_qsort(sorter, numKeysLocal, keys, direction); PROJ2CHK(err);
#else
  // err = Proj2SorterSortLocal_swenson_merge_sort(sorter, numKeysLocal, keys, direction); PROJ2CHK(err);
  err = Proj2SorterSortLocal_swenson_quick_sort(sorter, numKeysLocal, keys, direction); PROJ2CHK(err);
#endif
  return 0;
}
